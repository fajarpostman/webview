package id.co.earworks.mercedesbenzgiicomvecdaftarpeserta;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class Launcher extends AppCompatActivity {

    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        imageView = (ImageView) findViewById(R.id.giicomvec);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.transisi);
        imageView.startAnimation(animation);
        final Intent intent = new Intent(this, MainActivity.class);
        Thread thread = new Thread()
        {
            public void run() {
                try {
                    sleep(5000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                finally {
                    startActivity(intent);
                    finish();
                }
            }
        };
        thread.start();
    }
}
